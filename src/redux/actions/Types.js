export const EMAIL_CHANGED = 'EMAIL_CHANGED';
export const PASSWORD_CHANGED = 'PASSWORD_CHANGED';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAIL = 'LOGIN_USER_FAIL';
export const LOGOUT_USER = 'LOGOUT_USER';
export const LOADING = 'LOADING';
export const IMAGE_UPLOAD_LOADING = 'IMAGE_UPLOAD_LOADING';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

export const REGISTER_UPDATE = 'REGISTER_UPDATE';
export const LOGIN_UPDATE = 'LOGIN_UPDATE';
export const RESET_UUID = 'RESET_UUID';
export const GET_ERRORS = 'GET_ERRORS';
export const GET_MESSAGE = 'GET_MESSAGE';
export const INITIALIZE_USER = 'INITIALIZE_USER';

export const PETITION_VIEW = 'PETITION_VIEW';
export const ACTIVITY_VIEW = 'ACTIVITY_VIEW';
export const POST_VIEW = 'POST_VIEW';
export const NOTIFICATION_VIEW = 'NOTIFICATION_VIEW';
export const MESSAGE_LIST_VIEW = 'MESSAGE_LIST_VIEW';
export const PROFILE_VIEW = 'PROFILE_VIEW';
export const EMPTY_STATE = 'EMPTY_STATE';

// Profile actions types
export const PROFILE_UPDATE = 'PROFILE_UPDATE';
export const PROFILE_UPDATE_SUCCESS = 'PROFILE_UPDATE_SUCCESS';
export const GET_MY_PROFILE = 'GET_MY_PROFILE';
export const GET_USER_PROFILE = 'GET_USER_PROFILE';


// Post actions types
export const POST_UPDATE = 'POST_UPDATE';
export const GET_MY_POST = 'GET_MY_POST';
export const GET_POSTS = 'GET_POSTS';
export const SET_ACTIVE_POST = 'SET_ACTIVE_POST';

// Post actions types
export const ARTICLE_UPDATE = 'ARTICLE_UPDATE';
export const GET_MY_ARTICLE = 'GET_MY_ARTICLE';
export const GET_ARTICLES = 'GET_ARTICLES';
export const SET_ACTIVE_ARTICLE = 'SET_ACTIVE_ARTICLE';


// Poll actions types
export const POLL_UPDATE = 'POLL_UPDATE';
export const GET_POLLS = 'GET_POLLS';
export const SET_ACTIVE_POLL = 'SET_ACTIVE_POLL';
export const FILTER_POLLS = 'FILTER_POLLS';
export const FILTER_PARTICIPATED_POLLS = 'FILTER_PARTICIPATED_POLLS';

// Friend action types
export const GET_MY_FRIENDS = 'GET_MY_FRIENDS';
export const GET_PENDING_FRIEND_REQUESTS = 'GET_PENDING_FRIEND_REQUESTS';
export const GET_BLOCKED_FRIEND_REQUESTS = 'GET_BLOCKED_FRIEND_REQUESTS';
export const GET_USERS = 'GET_USERS';
export const SEARCH_USERS = 'SEARCH_USERS';

//Coment action types

export const COMMENT_UPDATE = 'COMMENT_UPDATE';
export const GET_COMMENTS = 'GET_COMMENTS';
export const COMMENT_CREATE_SUCCESS = 'COMMENT_CREATE_SUCCESS';

export const TOGGLE_SEARCH_BAR = 'TOGGLE_SEARCH_BAR';

// Location action types
export const GET_ALL_COUNTRIES = 'GET_ALL_COUNTRIES';
export const GET_ALL_STATES = 'GET_ALL_STATES';
export const GET_LGAS = 'GET_LGAS';

// share content types
export const TOGGLE_SHARE_MODAL = 'TOGGLE_SHARE_MODAL';
export const SET_SHARABLE_CONTENT = 'SET_SHARABLE_CONTENT';

export const REQUEST_UPGRADE_UPDATE = 'REQUEST_UPGRADE_UPDATE';